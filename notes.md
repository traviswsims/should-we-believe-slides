# Should We Believe in God?

## Charles Darwin, Letter to William Graham Down, July 3, 1881

  > With me the horrid doubt always arises whether the convictions of man's mind,
    which has been developed from the mind of lower animals,
    are of any value or at all trustworthy.
    Would anyone trust the convictions of a monkey's mind?

## Richard Dawkins, *River Out of Eden: A Darwinian View of Life*
  > In a universe of blind physical forces and genetic
  > replication, some people are going to get hurt, other people
  > are going to get lucky, and you won't find any rhyme or reason
  > in it, nor any justice... There is, at bottom,
  > no design, no purpose, no evil and no good, nothing but
  > blind, pitiless indifference... DNA neither cares nor knows.
  > DNA just is. And we dance to its music.

## Josephus, *The Antiquities of the Jews*

  > At this time there was a wise man who was called Jesus.
    And his conduct was good, and he was known to be virtuous.
    And many people from among the Jews and the other nations became his disciples.
    Pilate condemned him to be crucified and to die.
    And those who had become his disciples did not abandon his discipleship.
    They reported that he had appeared to themafter his crucifixion and that he was alive;
    accordingly, he was perhaps the Messiah concerning whom the prophets have recounted wonders.

## Pliny the Younger, Letter to Trajan, ca. 112 AD

  > [The Christians] were in the habit of meeting on a certain fixed day before it was light,
    when they sang in alternate verses a hymn to Christ, as to god, and bound themselves
    by a solumn oath, not to any wicked deeds, but never to commit any fraud,
    theft, or adultery, never to falsify their word, nor deny a trust when it should be called
    upon to deliver it up; after which, it was their custom to separate, and then
    reassemble to partake of food -- but food of an ordinary and innocent kind.

  Goes on to say Christians are all ranks of society, ages, sexes, and from the city and country


## Resurrection

### Minimal Facts

Gary Habermas, 1,400+ critical scholars, 1975 to present

  1. Jesus died by Roman crucifixion
  2. Buried in a tomb by Joseph of Arimathea
  3. Disciples were profoundly despondant, and lost hope
  4. Jesus' tomb was found empty very soon after his burial
  5. Disciples had experiences they believed were actual appearances of the risen Christ
  6. Disciples became convinced, willing to die for belief in resurrection
  7. Resurrection proclaimed from the beginning of church history


### Found by a Woman

Josephus, *Antiquities of the Jews* (Book 4, Section 219)

> Let not the testimony of women be admitted [in court], on account of the levity and boldness of their sex.  Nor let servants be admitted to give testimony, on account of the ignobility of their soul.

From the Talmud, central text of Rabbinical Judaism:

> Sooner let the words of the Law be burnt than delivered to a woman! (Sotah 19a)

> Happy is he whose children are male, but unhappy is he whose children are female! (Kiddushin 82b)

Daily prayer of Jewish men included this benediction:

> Blessed are you, Lord our God, ruler of the universe, who has not created me a woman! (Berachos 60b)





### Radical teaching
  Mark 3:21, 31 - family thought he was "out of his mind"

















# WLC - On Guard

## The empty tomb

  1.  He was buried (p 220)

    1. Disciples would not have believed in his ressurection with the body in the tomb
    2. Nobody would have become a Christian with an occupied tomb and sermons about the empty tomb
    3. It would have been trivially easy for Jewish authorities and skeptics to disprove -- just open the tomb!
        1.  Authorities were deeply troubled by new Christianity, as evidenced by persecution (e.g., Saul of Tarsus)
        2.  Decomposed (unrecognizable) remains would have still been a major obstacle to belief, with the burden of proof on the disciples
        3.  No body ever produced!  No body ever even *claimed* produced!

  2.  Buried by Joseph of Arimatha, a member of the Jewish Sanhedrin. (p 224)
      1.  Christians wouldn't have made it up
          1.  Court officials were well known, since the early Christians were Jews
          2.  Christians were hostile to the Sanhedrin -- they arranged a judical murder of Jesus, and were considerd *the* crucifiers (Acts 2:23, 36, 4:10)
          3.  A member of the Sandhedrin honoring Jesus when the disciples fled would have been the *last* object of Christian invention
          4.  Empty tomb "one of the earliest and best-attested facts about Jesus" - John A.T. Robinson, Cambridge University

  3.  Discovery of empty tomb well attested by multiple, independent, early sources (p 225)
      1.  Early Tradition 1 Corinthians 15:3-5
      2.  Mark's story is the early and simple
      3.  Discovered by women -- ouch!
      4.  Jews claimed the disciples stole the body! (Matt 28:11-15)

  4.  Disciples saw the risen Christ
      1.  Peter:  Luke 24:34, 1 Cor 15
      2.  The Twelve:  Luke 24:36-42, John 20:19-20
          1.  Physical resurrection
          2.  Same Jesus who was killed
      3.  500 Witnesses
          1.  Challenge issued by Paul -- go talk to them!
          2.  He knew them, since he knew some had died
          3.  Likely have been outdoors, since there were so many people
          4.  Possibly foretold by the angel in Matt 28:16-17
          5.  Not recorded in gospels, centered on Jerusalem => probably took place in Galilee
      3.  James (p 233)
          1. Younger brother of Jesus, had not believed in him (Mark 3:21, 31-35; Jn 7:1-10) -- embarrassing, wouldn't be made up!  Mark 3:21, 31 - family thought he was "out of his mind"
          2. With the apostles in the upper room in Acts 1:14
          3. Peter's first words when he was delivered from prison: "report this to James" (Acts 12:17)
          4. Paul refers to James as a pillar of the church in Jerusalem (and Peter, John), Gal 2:9
          5. Acts 21:18 James is head of council of elders in Jerusalem church
          6. Viewed on par with the apostles, 1 Cor 9:5
          7. What would it take to get you to believe you brother was God?! 1 Cor 15
      3.  Saul of Tarsus (p 235)
        1.  Told 3 times in Acts (e.g., 9:1-9); all over Paul's letters (1 Cor 15)
        2.  Respected Jewish leader, hated Christianity, beat and killed Christians
        3.  Xp brought poverty, labor, suffering, whipping, beating, stoning, shipwreck, etc.
        4.  Martryred in Rome, under Nero ca. 62, because he saw "Jesus our Lord" (1 Cor 9:1)
