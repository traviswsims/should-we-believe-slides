#= require_tree .

KEYS =
  up:    38,
  down:  40,
  enter: 13,
  tab:   9,
  two:   50,
  four:  52,
  six:   54,
  eight: 56,
  w: 87,
  a: 65,
  s: 83,
  d: 68,

KEYDOWN =
  "#{KEYS.up}":    Reveal.left,
  "#{KEYS.down}":  Reveal.right,
  "#{KEYS.tab}":   Reveal.up,
  "#{KEYS.enter}": Reveal.down,
  "#{KEYS.two}":   Reveal.down,
  "#{KEYS.four}":  Reveal.left,
  "#{KEYS.six}":   Reveal.right,
  "#{KEYS.eight}": Reveal.up,
  "#{KEYS.w}":     Reveal.up,
  "#{KEYS.a}":     Reveal.left,
  "#{KEYS.s}":     Reveal.down,
  "#{KEYS.d}":     Reveal.right,

Reveal.initialize
  progress: false,
  width: 800,
  height: 600

# $(window).on 'keydown', (e) ->
#   if fn = KEYDOWN[e.keyCode]
#     fn()
#     e.preventDefault()
